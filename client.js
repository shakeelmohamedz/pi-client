// Need to have this script run as root for copying videos

var Buffer = require("buffer").Buffer;
var fs = require("fs");
var fse = require("fs-extra");
var path = require("path");
var spawn = require("child_process").spawn;
var request = require("request");
var async = require("async");
var md5 = require("nodejs-md5");
// var ps = require("ps-man");
var Omx = require("node-omxplayer");

// var dest = path.resolve("/mnt/usbdrive0"); // USB path
var dest = path.resolve("/home/pi/videos");
var repoDir = "/home/pi/pi-client";
var tempdir = path.join(repoDir, "temp");
var loadingVideo = "/home/pi/loading.mp4";

// STATE variables
var currentVideo = {
    path: loadingVideo,
    md5: "TODO: implement me"
};
var mac = getMAC();
// Default to every 5 minutes
var defaultFetchInterval = 1000 * 60 * 5;
var fetchInterval = defaultFetchInterval;
var intervalID = null;

function getMAC() {
    return fs.readFileSync("/sys/class/net/eth0/address").toString().replace("\n", "");
}

function update(done) {
    var proc = spawn("git --git-dir=" + repoDir + "/.git --work-tree=" + repoDir + " pull && mkdir -p " + repoDir + "/node_modules && npm install --prefix " + repoDir, [], {shell: true});

    var updated = false;
    var stdout = "";
    proc.stdout.on("data", function(data) {
        var chunk = Buffer.from(data).toString();
        console.log("Update() got data");
        stdout += chunk + " ";
        // Success when data -> "Updating <sha>..<sha>"
        updated = updated || (chunk.indexOf("Updating ") === 0);
        if (chunk.indexOf("Already up-to-date.") === 0) {
            updated = false;
        }
    });

    var aborted = false;
    var stderr = "";
    proc.stderr.on("data", function(data) {
        var chunk = Buffer.from(data).toString();
        stderr += chunk + " ";
        aborted = aborted || (chunk.indexOf("Aborting") !== -1) || (chunk.indexOf("error: Your local changes") !== -1) || (chunk.indexOf("overwritten by merge") !== -1);
    });

    var errs = [];
    proc.on("error", function(err) {
        console.log("Update ERROR", err);
        errs.push(err);
    });

    proc.on("exit", function(code) {
        if (errs.length === 0) {
            errs = null;
        }
        else {
            errs = errs.join(" ");
        }

        stdout = "Code: " + code + "\n" + stdout;
        var success = updated && !aborted;
        done(errs || null, stdout, stderr, success);
    });
}

// TODO: add simple logging to file & console via util log methods
// ie: fs.writeFileSync(timestamp + ".txt", msg); console.log(msg);

function downloadVideo(v, done) {
    var filename = v.filename;
    console.log("downloading " + filename);
    var tempdest = path.join(tempdir, filename);
    request.get(v.url).pipe(fs.createWriteStream(tempdest)).on("error", function(err) {
        done(err);
    }).on("close", function() {
        // TODO: md5 verification before copying the file
        try {
            fse.copySync(tempdest, path.join(dest, filename));
            done();
        }
        catch (ex) {
            done(ex);
        }
    });
}

function getFileHashesOfDir(p, allDone) {
    // TODO: small bug here in the case where duplicate videos w/ different names exist
    // we'll only delete the last one we find since the path will be replaces... simple hashtable collision
    var fileHashes = {};

    var files = fs.readdirSync(p);

    // TODO filter out non-videos
    async.each(files,
        function(file, done) {
            console.log("----debug file:", file);
            var filePath = path.join(dest, file);
            md5.file.quiet(filePath, function(err, hash) {
                if (err) {
                    return done("Unable to hash file", file, "filePath", filePath);
                }
                fileHashes[hash] = filePath;
                done();
            });
        },
        function(err) {
            console.log("fileHashes --- early", fileHashes);
            allDone(err, fileHashes);
        }
    );
}

function clearFolder(folder) {
    var files = fs.readdirSync(folder);
    for (var f in files) {
        if (files[f] === "readme.txt") {
            continue;
        }
        fs.unlinkSync(path.join(folder, files[f]));
    }
}

var requestOpts = {
    url: "https://vidmanplayer.com/device/" + mac,
    json: true
};

// TODO: run "determine which video to play" logic here - ie: default to loading or pick first one in video dir
// TODO: handle stdout/stderr, etc like the others
spawn("killall omxplayer omxplayer.bin", [], {shell: true});
// input, output, loop
var player = Omx(currentVideo.path, "both", true);
player.on("error", function(err) {
    console.error("node-omxplyer error", err);
});

function run() {
    var body = {};
    var shouldUpdate = false;
    async.waterfall([
        function(done) {
            request.get(requestOpts, done);
        },
        function(resp, _body, done) {
            body = _body;
            if (body.message) {
                done(body.message);
            }
            else if (!body.hasOwnProperty("videos")) {
                done(new Error("Unexpected server response"));
            }
            else {
                if (body.hasOwnProperty("update")) {
                    shouldUpdate = !!body.update;
                }

                // TODO: this if block is "legacy" we can rip it out shortly
                if (body.hasOwnProperty("updateInterval") && typeof body.updateInterval === "number") {
                    fetchInterval = body.updateInterval;
                    console.log("Using fetchInterval of", fetchInterval);
                }
                else if (body.hasOwnProperty("fetch") && typeof body.fetch === "object") {
                    if (body.fetch.format === "msInterval" && body.fetch.hasOwnProperty("value")) {
                        fetchInterval = body.fetch.value;
                        try {
                            fetchInterval = parseInt(fetchInterval, 10);
                            clearInterval(intervalID);
                            intervalID = setInterval(run, fetchInterval);
                        }
                        catch (err) {
                            console.log("Couldn't parse", body.fetch.value, "as an int. Using 5 min update interval");
                            fetchInterval = defaultFetchInterval;
                        }
                    }
                    else if (body.fetch.format === "cron") {
                        // TODO: implement cron format
                    }
                    else {
                        console.log("Received unexpected body.fetch");
                    }
                }
                else {
                    console.log("Didn't get fetch information, payload was", body);
                }
                console.log("Got list of these videos", body.videos);
                // TODO: rename hashes since it's really a dict<hash,path>
                getFileHashesOfDir(dest, done);
            }
        },
        function(hashes, done) {
            var toDownload = [];
            for (var i = 0; i < body.videos.length; i++) {
                var cur = body.videos[i];
                var skip = false;
                for (var j in hashes) {
                    // j is the key (hash), hashes[j] is the value (path)
                    if (hashes.hasOwnProperty(j) && cur.md5.toLowerCase() === j.toLowerCase()) {
                        skip = true;
                        break;
                    }
                }
                if (!skip) {
                    toDownload.push(cur);
                }
            }

            console.log("Downloading these videos", toDownload);

            // Short circuit if there's nothing to do
            if (toDownload.length === 0) {
                if (body.videos.length === 0) {
                    clearFolder(dest);
                    console.log("Deleting all videos since none were assigned");
                }
                // TODO: bug here for case of multiple videos - nothing to download but something to delete
                return done(null);
            }

            // TODO: this .each() can be decomposed a bit for readability
            // Now we can start downloading...
            async.each(toDownload, downloadVideo, function(saveErr) {
                if (saveErr) {
                    console.error("Error downloading file", saveErr);
                    return;
                }

                // TODO: this is only deleting 1 video...
                // Figure out which files should be deleted based on server response + hashes
                console.log("debug--- hashes", hashes);
                var toDelete = [];
                for (var j in hashes) {
                    // j is the key (hash), hashes[j] is the value (path)
                    if (!hashes.hasOwnProperty(j)) {
                        continue;
                    }
                    var purge = true;
                    for (var i = 0; i < body.videos.length; i++) {
                        if (j.toLowerCase() === body.videos[i].md5.toLowerCase()) {
                            purge = false;
                            break;
                        }
                    }
                    if (purge) {
                        toDelete.push(hashes[j]);
                    }
                }

                console.log("Deleting the following unassigned videos", toDelete);
                // Delete files that shouldn't be there
                for (var f = 0; f < toDelete.length; f++) {
                    fs.unlinkSync(toDelete[f]);
                }

                // TODO: however, we may want to run 'killall omxplayer omxplayer.bin node' in some situations -- like when performing an update
                done();
                // var options = {};
                // ps.list(options, function(err, result) {
                //     // result : Object[]
                //     async.each(result,
                //         function(proc, eachDone) {
                //             var cur = proc;
                //             if (cur && cur.command && cur.command.indexOf("omxplayer") !== -1 && cur.pid > 0) {
                //                 console.log("trying", cur);
                //                 var o = {
                //                     pidList: [cur.pid],
                //                     signal: "-9"
                //                 };

                //                 ps.kill(o, function(err) {
                //                     console.log("killing", o);
                //                     if (err) {
                //                         console.log(err);
                //                     }
                //                     eachDone();
                //                 });
                //             }
                //             else {
                //                 eachDone();
                //             }
                //         },
                //         function(eachErr) {
                //             console.log("Running each done");
                //             console.error(eachErr || err);
                //             done(eachErr || err);
                //         }
                //     );
                // });
            });
        }],
        function(err) {
            console.log("hit done for chain", err);
            if (err) {
                return console.error(err);
            }
            else {
                // TODO: add support for multi-video playback
                // Play the first file we find, or loading vid if nothing else is available
                var videos = fs.readdirSync(dest);
                var vidPath = loadingVideo;
                if (videos.length > 0) {
                    vidPath = path.join(dest, videos[0]);
                }
                console.log("playing...", vidPath);

                // TODO: change this condition to use md5 hashes instead of filepaths
                if (currentVideo.path === vidPath) {
                    console.log("Already playing", currentVideo.path);
                }
                else {
                    currentVideo.path = vidPath;
                    // input, output, loop
                    player.newSource(vidPath, "both", true);
                }

                if (shouldUpdate) {
                    update(function(err, stdout, stderr, updated) {
                        if (err) {
                            console.log("Update process failed.");
                        }
                        else {
                            console.log("Update process succeeded.");
                            if (updated) {
                                // TODO: if the update succeeded, then we need to restart the proc
                                // TODO: peek at stdout/stderr/exit for this call -- but really it should be irrelevant since this proc will die too.
                                spawn("sudo systemctl restart vidman", [], {shell: true});
                            }
                        }
                        console.log("stdout:", stdout);
                        console.log("stderr:", stderr);
                    });
                }
            }
        }
    );
}

/**
 * Actual program code below
 *
 * TODO: break up run into distinct steps
 * 1) Fetch + download (possibly restart)
 * 2) Delete old videos if necessary
 * 3) Play video
 */

// Start with a clean slate
clearFolder(tempdir);
console.log("Device MAC address:", mac);

// Run once immediately - mainly so we move beyond the loading video
// TODO: refactor so the waterfall "done" callback logic is called instead
// Play the loading video for 3 seconds before trying to change videos
setTimeout(run, 3000);
intervalID = setInterval(run, fetchInterval);
