#!/bin/sh

# get rid of the cursor so we don't see it when videos are running
setterm -cursor off

# set here the path to the directory containing your videos
VIDEOPATH="/mnt/usbdrive0" 

# you can normally leave this alone
SERVICE="omxplayer"

# now for our infinite loop!
while true; do
        if ps ax | grep -v grep | grep $SERVICE > /dev/null
        then
       # echo 'fail' > /dev/null
        sleep 0;
else
        for entry in $VIDEOPATH/*
        do
                #echo $entry 
                clear
                omxplayer -b "$entry" > /dev/null
        done
fi
done
