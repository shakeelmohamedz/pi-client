#!/bin/sh

# disable window manager GUI (only needed for hypriot)
systemctl stop lightdm

# get rid of the cursor so we don't see it when videos are running
setterm -cursor off

# set here the path to the directory containing your videos
#VIDEOPATH="/mnt/usbdrive0"
VIDEOPATH="/home/pi/videos" 

# On start try to fetch videos right away
node /home/pi/pi-client/client.js

SERVICE="omxplayer"

# now for our infinite loop!
while true; do
    if ps ax | grep -v grep | grep $SERVICE > /dev/null
    then
        echo 'A video is playing' > /dev/null
    elif test -z "$(ls -A $VIDEOPATH)"
    then
        # No videos exist in $VIDEOPATH, so play the loading video
        omxplayer -b --no-osd "/home/pi/loading.mp4" > /dev/null
    else
        for entry in $VIDEOPATH/* # Just use first video file we find
        do
            clear
            # -b = black background, --loop loop video, --no-osd no seek overlay
            omxplayer -b --loop --no-osd "$entry" > /dev/null
        done
    fi
done
