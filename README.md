# Vidman

## Install

* Register this device
* Clone this repo with git in `$HOME` 
* `npm install`
* `cp ./vidman.conf /etc/supervisor/conf.d/vidman.conf`

### Development

* `npm test` to run eslint
* `codeclimate analyze` to run a more thorough code quality analysis
  - pre-requisite: `brew tap codeclimate/formulae && brew install codeclimate`